import app from './App';

app.get('/scenes/main', (req, res) => {
  res.send(<Main />);
});

const versions = [
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
  {
    title: 'asdf',
  },
];
const list = ['1', '2', '3'];

function VersionSelect() {
  return (
    <select class="form-select">
      {list.map((v) => (
        <option value={v}>{v}</option>
      ))}
    </select>
  );
}

function VersionList() {
  return (
    <ul class="list-group flex-fill" style="overflow-y: auto; height: 0">
      {versions.map((v) => (
        <li class="list-group-item">
          <span>{v.title}</span>
        </li>
      ))}
    </ul>
  );
}

function Main() {
  return (
    <div
      class="container-fluid d-flex flex-column flex-fill"
      style="padding: 24px; background-color: var(--bs-gray-300)">
      <div
        class="d-flex flex-column flex-fill"
        style="background-color: var(--bs-light); border-radius: 8px;">
        <ul class="nav nav-tabs" role="tablist">
          <li class="nav-item" role="presentation">
            <a
              class="nav-link active"
              role="tab"
              data-bs-toggle="tab"
              href="#tab-1">
              Translate
            </a>
          </li>
          <li class="nav-item" role="presentation">
            <a class="nav-link" role="tab" data-bs-toggle="tab" href="#tab-2">
              Versions
            </a>
          </li>
        </ul>
        <div class="tab-content" style="height: 100%; padding: 4px">
          <div
            id="tab-1"
            class="tab-pane active"
            role="tabpanel"
            style="height: 100%;">
            <form style="height: 100%;">
              <div class="row" style="height: 100%;">
                <div class="col d-flex flex-column">
                  <div class="input-group" style="margin-bottom: 4px">
                    <span class="input-group-text">Version</span>
                    <VersionSelect />
                  </div>
                  <div class="input-group flex-fill">
                    <span class="input-group-text">Stacktrace</span>
                    <textarea
                      class="form-control"
                      style="resize: none"></textarea>
                    <button class="btn btn-primary" type="button">
                      Translate
                    </button>
                  </div>
                </div>
                <div class="col">
                  <textarea
                    class="form-control"
                    style="height: 100%; resize: none"
                    readonly></textarea>
                </div>
              </div>
            </form>
          </div>
          <div
            id="tab-2"
            class="tab-pane"
            role="tabpanel"
            style="height: 100%;">
            <div class="d-flex flex-column" style="height: 100%;">
              <form style="padding-bottom: 4px">
                <div class="input-group">
                  <span class="input-group-text">Version Name</span>
                  <input class="form-control" type="text" />
                  <button class="btn btn-primary" type="button">
                    Generate
                  </button>
                </div>
              </form>
              <VersionList />
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Main;
