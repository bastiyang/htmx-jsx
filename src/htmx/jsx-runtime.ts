function parseProps(props: any) {
  if (props) {
    return Object.keys(props)
      .map((p) => (p !== 'children' ? ` ${p}="${props[p]}"` : ''))
      .join('');
  }
  return '';
}

function parseChildren(children: any) {
  if (children) {
    return children instanceof Array ? children.join('') : children;
  }
  return '';
}

export const jsx = (component: any, props: any) => {
  if (typeof component === 'function') {
    const componentResult = component(props);
    return componentResult;
  } else {
    const tagName = String(component) || 'no-tag';
    return props.children
      ? `<${tagName}${parseProps(props)}>${parseChildren(
          props.children,
        )}</${tagName}>`
      : `<${tagName}${parseProps(props)}></${tagName}>`;
  }
};

export const jsxs = jsx;
export default {
  jsx,
  jsxs: jsx,
};
