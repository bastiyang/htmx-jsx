declare namespace JSX {
  interface Element {
    children?: any;
    [key: string]: any;
  }
  interface IntrinsicElements {
    [key: string]: JSX.Element;
  }
}
